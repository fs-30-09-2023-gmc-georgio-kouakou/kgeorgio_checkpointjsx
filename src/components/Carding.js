import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import imgNoel from "../img/RandomImage_noel.jpg"
import Description from '../components/Description'
import {Name} from '../components/Name'
import {FirstName} from '../components/Name'
import Price from '../components/Price'


function Carding() {
    return (
        <Card style={{ width: '18rem' }}>
            <Card.Img src={FirstName ? `${imgNoel}` : ""}/>
            <Card.Body>
                <Card.Title>Mon Card</Card.Title>
                <Card.Text>
                    Mon nom : <Name />
                </Card.Text>
                <Card.Text>
                    Mon prix : <Price />
                </Card.Text>
                <Card.Text>
                    Ma description : <Description />
                </Card.Text>
                <Button variant="primary">Go somewhere</Button>
            </Card.Body>

            <h5>
                {FirstName!=="" ? `Hello ${FirstName}` : "Hello There !"}
            </h5>
        </Card>
    );
}

export default Carding;